function multiplyMatrices(m1, m2) {
    let res = [];
    for (let i = 0; i < m1.length; i++) {
        res[i] = [];
        for (let j = 0; j < m2[0].length; j++) {
            let temp = 0;
            for (let k = 0; k < m1[0].length; k++) {
                temp += m1[i][k] * m2[k][j];
            }
            res[i][j] = temp;
        }
    }
    return res;
}

var m1 = [[10,21],[13,48]]
var m2 = [[5,61],[7,8]]

var mResult = multiplyMatrices(m1, m2)

console.table(mResult) 