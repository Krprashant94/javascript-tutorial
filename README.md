# JavaScript Tutorial

Problem Index:
1. Create a JavaScript function to implement matrix multiplication.  ex A[m][n] x B[n][p] 
     - Use function to multiply columns and call it in async await style
2. Create a function to validate aadhar number of a person.
3. Create a function to validate email of a person.
4. Call both 2 and 3 function together(at same time) and print valid if both are valid
